//shared js file 
//basement code for the assigment, which is class code. This file is the foundation of the project to sotre information and it also incldues some functions 

//local storage variable
const TAXI_INFO_KEY = "TaxiTripTimeAndLocationInformation";
const TAXI_TRIP_KEY = "TaxiTripIndex";
const APP_DATA_KEY = "TaxiTripAppData";
const TAXI_STATEMENT = "TaxiStatement";

//trip class, the information for a single trip
class Trip{
    constructor(time,distance,taxi,fare,addresses,stop){
        this._id = "";
        this._startTimeAndDate=time;
        this._totalDistance=distance;
        this._taxiType=taxi;
        this._fare=fare;
        this._address=addresses;
        this._stop=stop;
    }
    get id(){
        return this._id;
    }
    get startTimeAndDate(){
        return this._startTimeAndDate;
    }
    get totalDistance(){
        return this._totalDistance;
    }
    get taxiType(){
        return this._taxiType;
    }
    get fare(){
        return this._fare;
    }
    get address(){
        return this._address;
    }
    get stop(){
        return this._stop;
    }
    
    set id(num){
        this._id = num;
    }
    set startTimeAndDate(date){
        this._startTimeAndDate = date;
    }
    set totalDistance(distance){
         this._totalDistance = distance;
    }
    set taxiType(type){
         this._taxiType = type;
    }
    set fare(prize){
         this._fare = prize;
    }
    set address(addressArray){
         this._address = addressArray;
    }
    addAdditionStop(additionStop){
        this._address.push(additionStop);
    }
    
    removeAdditionalStop(removeAddStop){
        this._address.splice(removeAddStop,1);
    }
    
    set stop(numStop){
        this._stop = numStop;
    }
    
    fromData(data){
        this._id = data._id;
        this._startTimeAndDate = data._startTimeAndDate;
        this._totalDistance = data._totalDistance;
        this._taxiType = data._taxiType;
        this._fare = data._fare;
        this._address = data._address;
        this._stop = data._stop;
    }
}

//triplist class, class for all of the trip
class TripList{
    constructor(){
        this._trips = [];
    }
    get trips(){
        return this._trips;
    }
    set trips(tripsArray){
        this._trips = tripsArray;
    }
    addTrip(time,distance,taxi,fare,addresses,stop){
        this._trips.push(new Trip(time,distance,taxi,fare,addresses,stop));
    }
    removeTrip(id){
        this._trips.splice(id,1);
    }
    fromData(data){
        this._trips = data._trips;
    }
}


//check localstorage data, check if data exist in local storage, input key and return ture or false
function checkLocalStorageDataExist(key){
    if (localStorage.getItem(key)){
        return true;
    }
    else{
        return false;
    }
}

//update the data in local storage
function updateLocalStorageData(key, data)
{
 localStorage.setItem(key,JSON.stringify(data));
}

//get data from local storage
function getLocalStorageData(key)
{
let data = JSON.parse(localStorage.getItem(key))
 return data;
}

//check local storage does exist
function checkLocalStorageDataExist(key)
{
    if (localStorage.getItem(key))
    {
        return true;
    }
    else{
        return false;
    }
}

//check the 1st date is before 2nd date, return ture if the data1 date is before data2, return false if the data1 date is after data2
//the input data type must be date type
function compareDateIsPast(data1,data2){
   let parsedDate1 = Date.parse(data1);
   let parsedDate2 = Date.parse(data2);
   if(parsedDate1 < parsedDate2){
       return true
   }
    else{
        return false
    }
}



//recovery class from local storage
let taxiTrip = new TripList();
if(checkLocalStorageDataExist(APP_DATA_KEY)){
    let dummyObj = getLocalStorageData(APP_DATA_KEY);
    taxiTrip.fromData(dummyObj);
}



let sedan = {
    name: "Sedan",
    Levy: 1.10
};
let suv = {
    name: "SUV",
    Levy: 3.50
};
let van = {
    name: "Van",
    Levy: 10.00
};


