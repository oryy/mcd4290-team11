//js file that runs the function for choose taxi type html page 

let loadOrder = "";

//data that get from local storage
let information = getLocalStorageData(TAXI_INFO_KEY);
let pickUpPoint = information.Addresses[0].results[0].formatted;
let time = new Date(information.Time);
let destinationPoint = information.Addresses[information.Addresses.length-1].results[0].formatted;
let displayTime = time.getFullYear()+"/"+(time.getMonth()+1) +"/"+time.getDate()+ "  " + time.getHours()+":"+time.getMinutes();
let additionalPoint=[];
let distance = getLocalStorageData(TAXI_INFO_KEY).Distance;
function displayPointAndTime(){
    //display pick up and destination point
    document.getElementById("pickup").innerHTML = pickUpPoint;
    document.getElementById("destination").innerHTML = destinationPoint;
    console.log(time);
    document.getElementById("dateAndTime").innerHTML=displayTime;

    //display addtional point
    let outputContent = "";
    if(information.Addresses.length>2){
        for(let i=1;i<information.Addresses.length-1;i++){
            let point = information.Addresses[i].results[0].formatted;
            additionalPoint.push(point);
            outputContent += '<P> Additional Stop' +i+ ':<label>'+point+'</label></P><br><br><br>';
        }
    }
    document.getElementById("AdditionalSpace").innerHTML=outputContent;
}

displayPointAndTime();


//document.getElementById("taxi1").innerHTML = calculateFare(sedan)

//Taxi fare culculation function
function calculateFare(taxiType){
    let fare = 4.20 + 1.622*distance + taxiType.Levy
    fare = fare.toFixed(2);
    if(information.Time.getHours>=17 || information.Time.getHours<=9){
        fare*1.2
    }
    return fare
}


function taxichoose(taxiType){
    let fare=calculateFare(taxiType);
    let alertContent = "Please confirm the selected taxi trip information\n\n\n"
    alertContent += "Pick up Location:" +pickUpPoint+"\n\nDestination Location:" + destinationPoint+"\n\n";
    if(additionalPoint.length>0){
        for (let i = 0; i < additionalPoint.length; i++){
            let number = i + 1;
            alertContent+="Additional Point "+number+": "+additionalPoint[i] + "\n\n"
        }
    }
     alertContent += "Time:" + displayTime + "\nNumber of Stops:" + information.numberOfstops +"\nDistance:"+distance.toFixed(2)+"\nTaxi Type:"+taxiType.name+"\nFare:"+calculateFare(taxiType)
    if(confirm(alertContent)){
        taxiTrip.addTrip(time,distance,taxiType.name,calculateFare(taxiType),information.Addresses,information.numberOfstops)
        updateLocalStorageData(APP_DATA_KEY, taxiTrip);
        updateLocalStorageData(TAXI_STATEMENT, "yes");
        location.replace("detail.html");
    }
    else{location.replace("main.html")}
}

document.getElementById("Fare1").innerText += calculateFare(sedan) +" dollars";
document.getElementById("Fare2").innerText += calculateFare(suv) + " dollars";
document.getElementById("Fare3").innerText += calculateFare(van) + " dollars";



