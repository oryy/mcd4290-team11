//js file
let tempArray = [];
//insertion sort depend trip starting date in ascending order, sort the tripsList array from the earliest to lastest
function insertionSort()
{
    tempArray = taxiTrip.trips
    var temp, j;
    tempArray[0]._id = 0
    for(var i = 1; i < tempArray.length; i++)
    {
        tempArray[i]._id = i;
        temp = tempArray[i];
        j = i;
        while( j > 0 && compareDateIsPast(temp._startTimeAndDate,tempArray[j-1]._startTimeAndDate))
        { 
            tempArray[j] = tempArray[j - 1];
            tempArray[j - 1] = temp;
            j--;
        }
    }
}
if(taxiTrip.trips.length>2){
    insertionSort();
    console.log(taxiTrip);
    setInterval(displayTrip(tempArray),1000);
}
else{
    tempArray = taxiTrip.trips;
    for(let i = 0; i < tempArray.length; i++){
        tempArray[i]._id = i;
    }
    setInterval(displayTrip(tempArray),1000);
}


// display trips on the html and differetiate past and future trip
function displayTrip(array){
    let futureTrips = '<ul class="demo-list-three mdl-list">';
    let pastTrips = '<ul class="demo-list-three mdl-list">';
    let currentTime = new Date();
    for(let i = 0; i < array.length; i++){
        let time = new Date(array[i]._startTimeAndDate);
        let displayTime = time.getFullYear()+"/"+(time.getMonth()+1) +"/"+time.getDate()+ "  " + time.getHours()+":"+time.getMinutes();
        let tripName = 'From'+array[i]._address[0].results[0].formatted+' to '+array[i]._address[array[i]._address.length-1].results[0].formatted;
        if(compareDateIsPast(array[i]._startTimeAndDate,currentTime)){
            pastTrips += '<li class="mdl-list__item mdl-list__item--three-line"><span class="mdl-list__item-primary-content"><i class="material-icons mdl-list__item-avatar">local_taxi</i><span>'+tripName+'</span><span class="mdl-list__item-text-body"><div class="mdl-grid"><div class="mdl-cell mdl-cell--3-col">Time: '+displayTime+'</div><div class="mdl-cell mdl-cell--1-col">Stops:'+array[i]._stop+'</div><div class="mdl-cell mdl-cell--2-col">Distance: '+ array[i]._totalDistance.toFixed(2)+'km </div><div class="mdl-cell mdl-cell--1-col">Fare: $'+array[i]._fare+' </div></div></span></span><span class="mdl-list__item-secondary-content"><a class="mdl-list__item-secondary-action" href="#"><button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" onclick="view('+array[i]._id+')">View</button></a></span></li>'
        }
        else{
            futureTrips += '<li class="mdl-list__item mdl-list__item--three-line"><span class="mdl-list__item-primary-content"><i class="material-icons mdl-list__item-avatar">local_taxi</i><span>'+tripName+'</span><span class="mdl-list__item-text-body"><div class="mdl-grid"><div class="mdl-cell mdl-cell--3-col">Time: '+displayTime+'</div><div class="mdl-cell mdl-cell--1-col">Stops:'+array[i]._stop+'</div><div class="mdl-cell mdl-cell--2-col">Distance: '+ array[i]._totalDistance.toFixed(2)+'km </div><div class="mdl-cell mdl-cell--1-col">Fare: $'+array[i]._fare+' </div></div></span></span><span class="mdl-list__item-secondary-content"><a class="mdl-list__item-secondary-action" href="#"><button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" onclick="view('+array[i]._id+')">View</button></a></span></li>'
        }
    }
    pastTrips+='</ul>';
    futureTrips+='</ul>';
    document.getElementById("past-panel").innerHTML = pastTrips;
    document.getElementById("future-panel").innerHTML = futureTrips;
}

//view button function, go to view detail information page, and store the index into local storage
function view(index){
    updateLocalStorageData(TAXI_TRIP_KEY,index);
    location.replace("detail.html");
}