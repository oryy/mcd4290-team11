//js file which will load the function in detail page


initMap(0, 0);
detailload();

let index=''

function detailload(){
    setTimeout(
        function load(){
            let loadOrderinfo = getLocalStorageData(TAXI_STATEMENT);
            let dataObejct = getLocalStorageData(APP_DATA_KEY);
            let pickUpA = document.getElementById("pick");
            let addiA  = document.getElementById("addi");
            let destA = document.getElementById("desti");
            let date = document.getElementById("date");
            let taxi = document.getElementById("taxi");
            let fare = document.getElementById("fare");
            let stops = document.getElementById("stops");
            let distan = document.getElementById("dis");
            if(loadOrderinfo == "yes"){
                index=taxiTrip.trips.length-1
                let tripdata = dataObejct._trips[dataObejct._trips.length - 1];
                console.log(tripdata);
                pickUpA.innerHTML += tripdata._address[0].results[0].formatted;
                if(tripdata._address.length > 2){
                    for(let i = 1; i < tripdata._address.length - 1; i++){
                    addiA.innerHTML += tripdata._address[i].results[0].formatted;
                    }
                }   
                else{addiA.innerHTML += " none"}
                destA.innerHTML += tripdata._address[tripdata._address.length - 1].results[0].formatted;
                let time=new Date(tripdata._startTimeAndDate)
                let displayTime = time.getFullYear()+"/"+(date.getMonth()+1) +"/"+time.getDate()+ "  " + time.getHours()+":"+time.getMinutes();
                date.innerHTML += displayTime;
                taxi.innerHTML += tripdata._taxiType;
                distan.innerHTML += tripdata._totalDistance.toFixed(3) + " KM";
                stops.innerHTML += tripdata._stop;
                fare.innerHTML += tripdata._fare + " dollars";
                map.flyTo({ center: [tripdata._address[0].results[0].geometry.lng, tripdata._address[0].results[0].geometry.lat], zoom: 10 });
                for(let i = 0; i < tripdata._address.length - 1; i++){
                    showRoute([tripdata._address[i].results[0].geometry.lng, tripdata._address[i].results[0].geometry.lat], [tripdata._address[i+1].results[0].geometry.lng,                               tripdata._address[i+1].results[0].geometry.lat],tripdata._address[i].results[0].annotations.MGRS,"red");
                }
                for(let i = 0; i < tripdata._address.length; i++){
                    let detailadd = tripdata._address[i].results[0].formatted
                    addPoint(tripdata._address[i].results[0].geometry.lng, tripdata._address[i].results[0].geometry.lat, "detail", detailadd);
            }
        updateLocalStorageData(TAXI_STATEMENT, "finish");
    }
    else{
        index=getLocalStorageData(TAXI_TRIP_KEY)
        let tripdata = taxiTrip.trips[getLocalStorageData(TAXI_TRIP_KEY)]
        console.log(tripdata);
        let date = document.getElementById("date");
        pickUpA.innerHTML += tripdata._address[0].results[0].formatted;
            if(tripdata._address.length > 2){
                for(let i = 1; i < tripdata._address.length - 1; i++){
                addiA.innerHTML += tripdata._address[i].results[0].formatted;
                }
             }   
            else{addiA.innerHTML += " none"}
            destA.innerHTML += tripdata._address[tripdata._address.length - 1].results[0].formatted;
            let time=new Date(tripdata._startTimeAndDate)
            let displayTime = time.getFullYear()+"/"+(time.getMonth()+1) +"/"+time.getDate()+ "  " + time.getHours()+":"+time.getMinutes();
            date.innerHTML += displayTime;
            taxi.innerHTML += tripdata._taxiType;
            distan.innerHTML += tripdata._totalDistance.toFixed(3) + " KM";
            stops.innerHTML += tripdata._stop;
            fare.innerHTML += tripdata._fare + " dollars";
            map.flyTo({ center: [tripdata._address[0].results[0].geometry.lng, tripdata._address[0].results[0].geometry.lat], zoom: 10 });
                for(let i = 0; i < tripdata._address.length - 1; i++){
                    showRoute([tripdata._address[i].results[0].geometry.lng, tripdata._address[i].results[0].geometry.lat], [tripdata._address[i+1].results[0].geometry.lng,                               tripdata._address[i+1].results[0].geometry.lat],tripdata._address[i].results[0].annotations.MGRS,"red");
                }
                for(let i = 0; i < tripdata._address.length; i++){
                    let detailadd = tripdata._address[i].results[0].formatted
                    addPoint(tripdata._address[i].results[0].geometry.lng, tripdata._address[i].results[0].geometry.lat, "detail", detailadd);
                } 
           
    }
            
        },100)
}


function deleateTrip(){
    taxiTrip.removeTrip(index)
    updateLocalStorageData(APP_DATA_KEY,taxiTrip);
    location.replace("view.html");
}

function showFare(taxiType){
    let fare = 4.20 + 1.622*distance + taxiType.Levy
    fare = fare.toFixed(2);
    let time=new Date(taxiTrip.trips[index].startTimeAndDate)
    if(time.getHours>=17 || time.getHours<=9){
        fare*1.2
    }
    return fare
}

function changeTaxi(){
    let currentTime=new Date()
    if(compareDateIsPast(taxiTrip.trips[index]._startTimeAndDate,currentTime)){
        alert("This trip is in the past, so you can't change the taxi type")
               document.getElementById("changeTaxi").disabled = true;
               document.getElementById("sedan").disabled = true;
               document.getElementById("suv").disabled = true;
               document.getElementById("van").disabled = true;
        }
    else{
        document.getElementById("sedan").disabled = false;
        document.getElementById("suv").disabled = false;
        document.getElementById("van").disabled = false;
        document.getElementById("sedan").innerHTML="Sedan\n Fare:"+showFare(sedan)
        document.getElementById("suv").innerHTML="SUV\n Fare:"+showFare(suv)
        document.getElementById("van").innerHTML="Van\n Fare:"+showFare(van)
    }
}

function selectTaxi(Type){
    if(confirm("Are you sure to select "+Type.name+" ?")){
        let newFare=showFare(Type)
        taxiTrip.trips[index]._fare=newFare
        taxiTrip.trips[index]._taxiType=Type.name
        updateLocalStorageData(APP_DATA_KEY,taxiTrip)
        alert("Change have been made")
        location.replace("view.html")

        
    }
}





