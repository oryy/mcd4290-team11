//script file, which will include majour function of the application. This file will take the responsibility of //running the program and fulfill the functionality







//global variables 
let beginMarker = "";
let dAta = "";
let stopS = [];
let currentMarkers = [];
let currentData = [];
let additionalStops = [];
let lineId = [];
let numberOfStops = 0;






// api
function getData(url, item, callback) {
    let QueryString = url + "?q=" + item + "&key=0eefba435ee84d469855f3d1c6951aa4" + "&jsonp="+callback;
    let script = document.createElement('script');
    script.src = QueryString;
    document.body.appendChild(script);
}




function reverseGeocoding(placeName,callBack){
    let uriAddress = encodeURI(placeName);
    let dAta = getData("https://api.opencagedata.com/geocode/v1/json", uriAddress, callBack);
}



function forwardGeocoding(lat, lng, callBack){
    let uriLat = encodeURI(lat);
    let uriLng = encodeURI(lng);
    let uriItem = uriLat + "+" + uriLng;
    let dAta = getData("https://api.opencagedata.com/geocode/v1/json", uriItem, callBack);
}







//function for initial address inout, call the map and place the marker on address 
function beginAddress(){
    firstLoad = "no"
    let address = document.getElementById("inputAddress").value;
    //additionalStops.push(address);
    reverseGeocoding(address, "callB");
}


function additionalAddress(){
    numberOfStops++;
    let address = document.getElementById("inputAddAddress").value;
    additionalStops.push(address);
    reverseGeocoding(address, "additioanlStopsCallback");
}


function destinationStop(){
    numberOfStops++;
    let address = document.getElementById("inputDesAddress").value;
    //additionalStops.push(address);
    reverseGeocoding(address, "destinaltioanlStopCallB");
    alert("You coould deleate any adiitional stops if you wish");
    
}


function deleateAdditionalStop(index){
    distance = 0;
    additionalStops.splice(index - 1,1);
    removeAllLayer();
    currentMarkers[index].remove();
    currentMarkers.splice(index,1);
    currentData.splice(index,1);
    numberOfStops = currentMarkers.length - 1;
    for(let i = 0; i <currentMarkers.length - 1; i++){
        showRoute([currentMarkers[i]._lngLat.lng, currentMarkers[i]._lngLat.lat],[currentMarkers[i+1]._lngLat.lng, currentMarkers[i+1]._lngLat.lat],currentData[i+1].results[0].annotations.MGRS,"red");
    }
    updateAdditionalStopChip();
}





//update the addtional stop information in html
function updateAdditionalStopChip(){
    var outputContent=""
        for(let i = 1; i < currentData.length - 1; i++){
            let addressName=currentData[i].results[0].formatted
            outputContent+='<div class="mdl-cell mdl-cell--6-col"><div>Additional stop '+i+'</div><span class="mdl-chip mdl-chip--deletable"><span class="mdl-chip__text">'+addressName+'</span><button type="button" class="mdl-chip__action" onclick="deleateAdditionalStop('+i+')"><i class="material-icons">cancel</i></button></span></div>'
        }
    
    document.getElementById("additionalDisplay").innerHTML = outputContent;
}
//setInterval(updateAdditionalStopChip,1000);





//click on comfirm button to save the input value as a object into local storage called "TaxiTripTimeAndLocationInformation", the variable for the local storage name is TAXI_INFO_KEY. Then go to chooseTaxiTyoe.html
function comfirmInput(){
    
    let pickUp = document.getElementById("inputAddress").value;
    let destination = document.getElementById("inputDesAddress").value;
    let dateAndTime = document.getElementById("inputDateAndTime").value;
    let inputInfo = {
        Addresses: currentData,
        Time: dateAndTime,
        Distance: distance,
        numberOfstops: numberOfStops
    }
    updateLocalStorageData(TAXI_INFO_KEY,inputInfo);
    location.replace("chooseTaxiType.html");
}
















































