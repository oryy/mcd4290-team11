// this file contain all the callbakc functions for the api, which also runs map code 

//callback for the begin point 
function callB(Data){
    currentData = [];
    currentMarkers = [];
    dAta = Data;
    currentData.push(dAta);
    let lng = dAta.results[0].geometry.lng;
    let lat = dAta.results[0].geometry.lat;
    initMap(lng, lat, 'map');
    addPoint(lng, lat, "yes");
}


//callback for additoanl stops 
function additioanlStopsCallback(Data){
    dAta = Data;
    currentData.push(dAta);
    let lng = dAta.results[0].geometry.lng;
    let lat = dAta.results[0].geometry.lat;
    showRoute([beginMarker._lngLat.lng, beginMarker._lngLat.lat], [lng, lat], dAta.results[0].annotations.MGRS, "red");
    lineId.push(dAta.results[0].annotations.MGRS);    
    addPoint(lng, lat, "no");
    map.flyTo({ center: [lng, lat], zoom: 15 });
    document.getElementById("inputAddAddress").value = "";
}

//callback for desrtional stop
function destinaltioanlStopCallB(Data){
    dAta = Data;
    currentData.push(dAta);
    let lng = dAta.results[0].geometry.lng;
    let lat = dAta.results[0].geometry.lat;
    showRoute([beginMarker._lngLat.lng, beginMarker._lngLat.lat], [lng, lat], dAta.results[0].annotations.MGRS, "red");
    lineId.push(dAta.results[0].annotations.MGRS);  
    addPoint(lng, lat, "destination");
    map.flyTo({ center: [lng, lat], zoom: 15 });
    updateAdditionalStopChip();
}
