// this file contians all the code to initial the map or any function which interacts with the map

// draw line
let distance = 0;
function showRoute(s, e, id, color) {
    //mao.removeLayer
    var route = {
        "id": "route",
        'type': 'FeatureCollection',
        'features': [
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'LineString',
                    'coordinates': [s, e]
                }
            }
        ]
    };

    let lineDistance = turf.lineDistance(route.features[0], 'kilometers');
    let stepArr = [];
    let steps = 20000;
    for (let i = 0; i < lineDistance; i += lineDistance / steps) {
        var lnglat = turf.along(route.features[0], i, 'kilometers');
        stepArr.push(lnglat.geometry.coordinates);
    }
    distance += parseFloat(lineDistance);
    map.addLayer({
        "id": id,
        "type": "line",
        "source": {
            "type": "geojson",
            "data": {
                "type": "Feature",
                "properties": {},
                "geometry": {
                    "type": "LineString",
                    "coordinates": stepArr
                }
            }
        },
        "layout": {
            "line-join": "round",
            "line-cap": "round",
        },
        "paint": {
            "line-color": color,
            "line-width": 2,
        }
    });
}


//things to do after drag the marker
function onDragEnd() {
    let lngLat = beginMarker.getLngLat()
    forwardGeocoding(lngLat.lat, lngLat.lng, "callB");
    document.getElementById("inputAddress").value = dAta.results[0].formatted;
    console.log(dAta.results[0].formatted);
}




//map initialation 
function initMap(c1, c2){
    mapboxgl.accessToken = 'pk.eyJ1IjoiYWxlY2NhaSIsImEiOiJja2xscWM0bmkwMXRmMnBxajVwcWVxNW8xIn0.4DSIVs1qG6P74Y7bm1tlsA';
    map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/light-v9',
        center: [c1, c2],
        zoom: 15
    });
}
//initMap(144.946457,-37.840935)

function addPoint(lon, lat, draggable, detailadd) {
    if(draggable == "yes"){
        let address = ""
        if(firstLoad == "yes"){
            address = dAta.results[0].formatted;
        }
        else{
            address = document.getElementById("inputAddress").value;
        }
        let popup = new mapboxgl.Popup()
        .setText(address)
        .addTo(map);
        beginMarker = new mapboxgl.Marker({draggable:true}).setLngLat([lon, lat])//marker
                    .addTo(map)
                    .setPopup(popup);
        let stop = [lon, lat];        
        stopS.push(stop);
        currentMarkers.push(beginMarker);
        
    //after draged the marker run onDragEnd function
        beginMarker.on('dragend', onDragEnd);
    }
    else if(draggable == "destination"){
        let address = document.getElementById("inputDesAddress").value;
        let popup = new mapboxgl.Popup()
        .setText(address)
        .addTo(map);
        beginMarker = new mapboxgl.Marker().setLngLat([lon, lat])//marker
                    .addTo(map)
                    .setPopup(popup);
        let stop = [lon, lat];
        stopS.push(stop);
        currentMarkers.push(beginMarker);
    }
    else if(draggable == "detail"){
        let address = detailadd;
        let popup = new mapboxgl.Popup()
        .setText(address)
        .addTo(map);
        beginMarker = new mapboxgl.Marker().setLngLat([lon, lat])//marker
                    .addTo(map)
                    .setPopup(popup);
    }
    else{
        let address = document.getElementById("inputAddAddress").value;
        let popup = new mapboxgl.Popup()
        .setText(address)
        .addTo(map);
        beginMarker = new mapboxgl.Marker().setLngLat([lon, lat])//marker
                    .addTo(map)
                    .setPopup(popup);
        let stop = [lon, lat];
        stopS.push(stop)
        currentMarkers.push(beginMarker);
        }
}



function removeAllLayer() {
    for (var i = 1; i < currentData.length; i++){
        let lineId = currentData[i].results[0].annotations.MGRS;
        map.removeLayer(lineId);
        map.removeSource(lineId);
    }
}


function deleatemarkers(){
    for(i = 0; i < currentMarkers.length; i++){
        currentMarkers[i].remove()
    }
}
